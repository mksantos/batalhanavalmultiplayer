"""
 Example program to show using an array to back a board.board on-screen.
 
 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/
 
 Explanation video: http://youtu.be/mdTeqiWyFnc
"""
import pygame
import socket
from gamestate import GameState 

def numberEnemies():
    text_nenemies = font.render("Navios inimigos restantes: " + str(14 - int(enemy.score)), 1, BLACK)
    text_nepos = text_nenemies.get_rect()
    text_nepos.center = [int(screen.get_rect().centerx + WINDOW_SIZE[0] / 4),  (MARGIN + HEIGHT) * (ROW + 1) - MARGIN * 2 + HEAD]
    screen.blit(text_nenemies, text_nepos)
    pygame.display.update()

def messageBottom(message):
    text_message = font.render(message, 1, BLACK)
    text_mpos = text_message.get_rect()
    text_mpos.center = [int(screen.get_rect().centerx - WINDOW_SIZE[0] / 4),  (MARGIN + HEIGHT) * (ROW + 1) - MARGIN * 2 + HEAD]
    screen.blit(text_message, text_mpos)
    pygame.display.update()

def messageTop(message, color):
    text_top = font.render(message, 1, color)
    text_tpos = text_top.get_rect()
    text_tpos.center = [screen.get_rect().centerx, int(screen.get_rect().centery / 2)]
    screen.blit(text_top, text_tpos)
    pygame.display.update()

def buttonUp(message, color):
    text_button = font.render(message, 1, color)
    text_bpos = text_button.get_rect()
    text_bpos.center = [screen.get_rect().centerx, screen.get_rect().centery]
    screen.blit(text_button, text_bpos)
    pygame.display.update()

def buttonDown(message, color):
    text_button = font.render(message, 1, color)
    text_bpos = text_button.get_rect()
    text_bpos.center = [screen.get_rect().centerx, int(screen.get_rect().centery + BUTTON_NEXT_HEIGHT / 2 + BUTTON_HEIGHT * 2)]
    screen.blit(text_button, text_bpos)
    pygame.display.update()

def printTab(color, width, height, text):
    if text != " ":
        text_owner = font.render(text, 1, color)
        text_opos = text_owner.get_rect()
        if text == "Seu Campo de Batalha":
            text_opos.center = [int(screen.get_rect().centerx + WINDOW_SIZE[0] / 4), int(HEAD * 1 / 3)]
        else:
            text_opos.center = [int(screen.get_rect().centerx - WINDOW_SIZE[0] / 4), int(HEAD * 1 / 3)]
        screen.blit(text_owner, text_opos)

    text_numbers = font.render("  0   1   2   3   4    5   6   7   8   9", 1, color)
    screen.blit(text_numbers,[LEFT_MARGIN + width, int(MARGIN + height)])
    screen.blit(font.render("A", 1, color), [MARGIN * 2 + width, HEAD + MARGIN * 2])
    screen.blit(font.render("B", 1, color), [MARGIN * 2 + width, HEAD + MARGIN * 3 + HEIGHT])
    screen.blit(font.render("C", 1, color), [MARGIN * 2 + width, HEAD + MARGIN * 4 + HEIGHT * 2])
    screen.blit(font.render("D", 1, color), [MARGIN * 2 + width, HEAD + MARGIN * 5 + HEIGHT * 3])
    screen.blit(font.render("E", 1, color), [MARGIN * 2 + width, HEAD + MARGIN * 6 + HEIGHT * 4])
    screen.blit(font.render("F", 1, color), [MARGIN * 2 + width, HEAD + MARGIN * 7 + HEIGHT * 5])
    screen.blit(font.render("G", 1, color), [MARGIN * 2 + width, HEAD + MARGIN * 8 + HEIGHT * 6])
    screen.blit(font.render("H", 1, color), [MARGIN * 2 + width, HEAD + MARGIN * 9 + HEIGHT * 7])
    screen.blit(font.render("I", 1, color), [MARGIN * 3 + width, HEAD + MARGIN * 10 + HEIGHT * 8])
    screen.blit(font.render("J", 1, color), [MARGIN * 2 + width, HEAD + MARGIN * 11 + HEIGHT * 9])
    pygame.display.update()

def printBoard(board, width, enemy):
    for row in range(ROW):
        for column in range(COLUMN):
            color = WHITE
            color_circle = RED
            if board[row][column] == 3:
                color = GREEN
                color_circle = BLACK
                pygame.draw.rect(screen, color, [(MARGIN + WIDTH) * column + MARGIN + LEFT_MARGIN + width, (MARGIN + HEIGHT) * row + MARGIN + HEAD, WIDTH, HEIGHT])
                pygame.draw.circle(screen, color_circle, [(MARGIN + WIDTH) * column + MARGIN + LEFT_MARGIN + width + int(WIDTH / 2), (MARGIN + HEIGHT) * row + MARGIN + HEAD + int(WIDTH / 2)], int(WIDTH / 2))
            else: 
                if board[row][column] == 1 and not enemy:
                    color = GREEN
                if board[row][column] == 2:
                    color = RED
                if board[row][column] == 4:
                    color = BLUE
                pygame.draw.rect(screen, color, [(MARGIN + WIDTH) * column + MARGIN + LEFT_MARGIN + width, (MARGIN + HEIGHT) * row + MARGIN + HEAD, WIDTH, HEIGHT])
    pygame.display.update()

# Cria o socket TCP/IP
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Faz o bind no endereco e porta
server_address = ('localhost', 5000)
sock.bind(server_address)

# Fica ouvindo por conexoes
sock.listen(1)

 
# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (141, 240, 145)
RED = (210, 72, 72)
BLUE = (52,52,170)
YELLOW = (255,255,100)
 
# This sets the WIDTH and HEIGHT of each board.board location
WIDTH = 30
HEIGHT = 30

# This sets the number of columns and rows in the board
COLUMN = 10
ROW = 10

# This sets the margin between each cell
MARGIN = 5

LEFT_MARGIN = 30
HEAD = 30
BOTTOM = 30
MESSAGE = 30

BUTTON_HEIGHT = 2 * HEIGHT
BUTTON_WIDTH = COLUMN * (WIDTH + MARGIN)

BUTTON_NEXT_HEIGHT = 2 * HEIGHT
BUTTON_NEXT_WIDTH = (COLUMN / 2) * (WIDTH + MARGIN)
 
# Loop until the user clicks the close button.
done = False
change = False
end = False
waiting = False
count = 0
play = 0

# Set wich frame will show 1: Start frame; 2: Put your name frame; 3: Put ships frame; 4: Rounds frame; 5: Win/Lose frame
frame = 1
name = ' '

color_button = GREEN
color_next_button = RED

# Initialize pygame
pygame.init()

# Used to manage how fast the screen updates
clock = pygame.time.Clock()
print('Aguardando a conexao do jogador')
connection, client_address = sock.accept()
print('Jogador chegou! :)') 
 
# Set the HEIGHT and WIDTH of the screen
WINDOW_SIZE = [(MARGIN + WIDTH) * COLUMN + MARGIN + LEFT_MARGIN, (MARGIN + HEIGHT) * ROW + MARGIN * 2 + HEAD]

screen = pygame.display.set_mode(WINDOW_SIZE)
 
# Set title of screen
pygame.display.set_caption("Player 1")

# -------- Main Program Loop -----------
while not done:
    
    #define os navios do tabuleiro
    board = GameState(10, "Server")
    enemy = GameState(10, "Client")
    tam = 10
    sub = [[1,3]] #tam 3
    cru = [[1,4]]
    arm_b = [[2,3]]
    air = [[4,3]]
    
    board.defineTab(sub, cru, arm_b, air)
    
    font = pygame.font.Font(None, 36)

    while frame == 1:
        messageTop("Jogo Batalha Naval", YELLOW)
        buttonUp("Começar o jogo", BLUE)

        for event in pygame.event.get():  # User did something
            if event.type == pygame.QUIT:  # If user clicked close
                done = True  # Flag that we are done so we exit this loop 
                break
            elif event.type == pygame.MOUSEBUTTONDOWN:
                # User clicks the mouse. Get the position
                pos = pygame.mouse.get_pos()
                # Change the x/y screen coordinates to board.board coordinates
                column = (pos[0] - LEFT_MARGIN) // (WIDTH + MARGIN)
                row = (pos[1] - HEAD) // (HEIGHT + MARGIN)
                if (pos[0] >= (screen.get_rect().centery - BUTTON_WIDTH / 2) and pos[0] <= (screen.get_rect().centery + BUTTON_WIDTH / 2)) and (pos[1] >= (screen.get_rect().centerx - BUTTON_HEIGHT / 2) and pos[1] <= (screen.get_rect().centerx + BUTTON_HEIGHT / 2)):
                    color_button = WHITE
                    color_next_button = GREEN
                    frame = 2
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    color_button = WHITE
                    color_next_button = GREEN
                    frame = 2
        # Set the screen background
        screen.fill(RED)

        # Button Start Game from frame 1 and space to put name in frame 2
        pygame.draw.rect(screen, color_button, [int(screen.get_rect().centerx - BUTTON_WIDTH / 2), int(screen.get_rect().centery - BUTTON_HEIGHT / 2), BUTTON_WIDTH, BUTTON_HEIGHT])

        clock.tick(10)

        pygame.display.flip()

    while frame == 2:
        messageTop("Digite seu nome:", BLUE)
        buttonDown("Continuar", BLUE)
        buttonUp(name, BLUE)

        for event in pygame.event.get():  # User did something
            if event.type == pygame.QUIT:  # If user clicked close
                done = True  # Flag that we are done so we exit this loop 
            elif event.type == pygame.MOUSEBUTTONDOWN:
                # User clicks the mouse. Get the position
                pos = pygame.mouse.get_pos()
                # Change the x/y screen coordinates to board.board coordinates
                column = (pos[0] - LEFT_MARGIN) // (WIDTH + MARGIN)
                row = (pos[1] - HEAD) // (HEIGHT + MARGIN)
               
                color_next_button = GREEN
                color_button = WHITE
                if (pos[0] >= (screen.get_rect().centery - BUTTON_NEXT_WIDTH / 2) and pos[0] <= (screen.get_rect().centery + BUTTON_NEXT_WIDTH / 2)) and (pos[1] >= (screen.get_rect().centerx + BUTTON_HEIGHT * 2) and pos[1] <= (screen.get_rect().centerx + BUTTON_HEIGHT * 2 + BUTTON_NEXT_HEIGHT)):
                    color_button = RED
                    color_next_button = RED
                    frame = 3
                    WINDOW_SIZE = [(MARGIN + WIDTH) * COLUMN + MARGIN + LEFT_MARGIN, (MARGIN + HEIGHT) * ROW + MARGIN * 2 + HEAD + BOTTOM]
                    screen = pygame.display.set_mode(WINDOW_SIZE)
                    for r in range(ROW):
                        for c in range(COLUMN):
                            board.board[r][c] = 0

            elif event.type == pygame.KEYDOWN:
                letra = chr(event.key)
                if event.key == 8:
                    name = name[:-1]
                else:
                    if (event.key >= 48 and event.key <= 57) or (event.key >= 65 and event.key <= 90) or (event.key >= 97 and event.key <= 122):
                        name = name + letra
        board.player = name

        # Set the screen background
        screen.fill(RED)
    
        # Button Next from frame 2
        pygame.draw.rect(screen, color_next_button, [int(screen.get_rect().centerx - BUTTON_NEXT_WIDTH / 2), screen.get_rect().centery + BUTTON_HEIGHT * 2, BUTTON_NEXT_WIDTH, BUTTON_NEXT_HEIGHT]) 

        # Button Start Game from frame 1 and space to put name in frame 2
        pygame.draw.rect(screen, color_button, [int(screen.get_rect().centerx - BUTTON_WIDTH / 2), int(screen.get_rect().centery - BUTTON_HEIGHT / 2), BUTTON_WIDTH, BUTTON_HEIGHT])

        clock.tick(10)

        pygame.display.flip()

    message = ''

    while frame == 3:
        if count < 4:
            message = 'Coloque ' + str(4 - count) + ' Submarinos'
        elif count < 8:
            message = 'Coloque ' + str(8 - count) + ' Cruzadores'
        elif count < 11:
            message = 'Coloque ' + str(11 - count) + ' Encouraçados'
        elif count <= 14:
            message = 'Coloque ' + str(14 - count) + ' Porta Aviões'

        text_message = font.render(message, 1, BLACK)
        text_mpos = text_message.get_rect()
        text_mpos.center = [screen.get_rect().centerx,  (MARGIN + HEIGHT) * (ROW + 1) - MARGIN * 2 + HEAD]
        screen.blit(text_message, text_mpos)

        printTab(BLUE, 0, 0, " ")

        pygame.display.update()

        for event in pygame.event.get():  # User did something
            if event.type == pygame.QUIT:  # If user clicked close
                done = True  # Flag that we are done so we exit this loop 
            elif event.type == pygame.MOUSEBUTTONDOWN:
                # User clicks the mouse. Get the position
                pos = pygame.mouse.get_pos()
                # Change the x/y screen coordinates to board.board coordinates
                column = (pos[0] - LEFT_MARGIN) // (WIDTH + MARGIN)
                row = (pos[1] - HEAD) // (HEIGHT + MARGIN)
                # Set that location to one
                if column < len(board.board) and row < len(board.board[0]) and row >= 0 and column >= 0:
                    if board.board[row][column] == 1:
                        board.board[row][column] = 0
                        count = count - 1

                    else:
                        board.board[row][column] = 1
                        count = count + 1

        if count == 4:
            count = 0
            frame = 4
            HEAD = HEAD * 3
            WINDOW_SIZE = [((MARGIN + WIDTH) * COLUMN + MARGIN + LEFT_MARGIN) * 2, (MARGIN + HEIGHT) * ROW + MARGIN * 2 + HEAD + BOTTOM]
            screen = pygame.display.set_mode(WINDOW_SIZE)

        # Set the screen background
        screen.fill(RED)

        # Button Next from frame 2
        pygame.draw.rect(screen, color_next_button, [int(screen.get_rect().centerx - BUTTON_NEXT_WIDTH / 2), screen.get_rect().centery + BUTTON_HEIGHT * 2, BUTTON_NEXT_WIDTH, BUTTON_NEXT_HEIGHT]) 

        # Button Start Game from frame 1 and space to put name in frame 2
        pygame.draw.rect(screen, color_button, [int(screen.get_rect().centerx - BUTTON_WIDTH / 2), int(screen.get_rect().centery - BUTTON_HEIGHT / 2), BUTTON_WIDTH, BUTTON_HEIGHT])

        clock.tick(10)

        # Draw the board.board
        printBoard(board.board, 0, False)

        pygame.display.flip()

    if frame == 4:

        connection.sendall(board.save().encode('utf-8'))

        printTab(BLUE, 0, HEAD * 2 / 3, "Campo de Batalha Inimigo")
        printTab(YELLOW, (MARGIN + WIDTH) * (COLUMN + 1), HEAD * 2 / 3, "Seu Campo de Batalha")
        messageBottom('Aguardando adversário')
        printBoard(enemy.board, 0, True)
        printBoard(board.board, (MARGIN + WIDTH) * (COLUMN + 1), False)

        data = connection.recv(2048)

        enemy.restore(data.decode('utf-8'))

    while frame == 4:
        while not waiting:
            
            numberEnemies()
            messageBottom('Sua vez de jogar')

            # Print the columns and rows of the game
            printTab(BLUE, 0, HEAD * 2 / 3, "Campo de Batalha Inimigo")
            printTab(YELLOW, (MARGIN + WIDTH) * (COLUMN + 1), HEAD * 2 / 3, "Seu Campo de Batalha")
            
            for event in pygame.event.get():  # User did something
                if event.type == pygame.QUIT:  # If user clicked close
                    done = True  # Flag that we are done so we exit this loop 
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    # User clicks the mouse. Get the position
                    pos = pygame.mouse.get_pos()
                    # Change the x/y screen coordinates to board.board coordinates
                    column = (pos[0] - LEFT_MARGIN) // (WIDTH + MARGIN)
                    row = (pos[1] - HEAD) // (HEIGHT + MARGIN)
                    # Set that location to one
                    if column < len(board.board) and row < len(board.board[0]) and row >= 0 and column >= 0:    
                        play = enemy.attack(row, column)
                        if play != 5:
                            # Envia o tabuleiro para o jogador
                            connection.sendall(enemy.save().encode('utf-8'))
                            waiting = True

                    #print("Click ", pos, "board coordinates: ", row, column)

                    if enemy.isFinished():
                        winner = "finish"
                        #envia mensagem para o outro player que ganhou o jogo
                        connection.sendall(winner.encode('utf-8'))
                        frame = 5
                        winner = True
                        HEAD = HEAD // 3
                        color_button = GREEN

                        WINDOW_SIZE = [(MARGIN + WIDTH) * COLUMN + MARGIN + LEFT_MARGIN, (MARGIN + HEIGHT) * ROW + MARGIN * 2 + HEAD]
                        screen = pygame.display.set_mode(WINDOW_SIZE)
            # Set the screen background
            screen.fill(RED)

            # Button Next from frame 2
            pygame.draw.rect(screen, color_next_button, [int(screen.get_rect().centerx - BUTTON_NEXT_WIDTH / 2), screen.get_rect().centery + BUTTON_HEIGHT * 2, BUTTON_NEXT_WIDTH, BUTTON_NEXT_HEIGHT]) 

            # Button Start Game from frame 1 and space to put name in frame 2
            pygame.draw.rect(screen, color_button, [int(screen.get_rect().centerx - BUTTON_WIDTH / 2), int(screen.get_rect().centery - BUTTON_HEIGHT / 2), BUTTON_WIDTH, BUTTON_HEIGHT])

            clock.tick(10)

            # Draw the enemy board and your board
            printBoard(enemy.board, 0, True)                        
            printBoard(board.board, (MARGIN + WIDTH) * (COLUMN + 1), False)

            pygame.display.flip()

            
        while waiting:
            printTab(BLUE, 0, HEAD * 2 / 3, "Campo de Batalha Inimigo")
            printTab(YELLOW, (MARGIN + WIDTH) * (COLUMN + 1), HEAD * 2 / 3, "Seu Campo de Batalha")
            messageBottom('Aguardando adversário')

            data = connection.recv(2048)

            if "finish" in data.decode('utf-8'):
                frame = 5
                winner = False
                HEAD = HEAD // 3
                WINDOW_SIZE = [(MARGIN + WIDTH) * COLUMN + MARGIN + LEFT_MARGIN, (MARGIN + HEIGHT) * ROW + MARGIN * 2 + HEAD]
                screen = pygame.display.set_mode(WINDOW_SIZE)
                break

            board.restore(data.decode('utf-8'))
            message = 'Sua vez de jogar'

            waiting = False

    while frame == 5:
        data = ''
        color_button = GREEN

        if winner:
            messageTop("Você Venceu!", YELLOW)
        else:
            messageTop("Você Perdeu :(", BLACK)

        buttonUp("Reiniciar", BLUE)

        for event in pygame.event.get():  # User did something
            if event.type == pygame.QUIT:  # If user clicked close
                done = True  # Flag that we are done so we exit this loop 
            elif event.type == pygame.MOUSEBUTTONDOWN:
                # User clicks the mouse. Get the position
                pos = pygame.mouse.get_pos()
                # Change the x/y screen coordinates to board.board coordinates
                column = (pos[0] - LEFT_MARGIN) // (WIDTH + MARGIN)
                row = (pos[1] - HEAD) // (HEIGHT + MARGIN)
                
                if (pos[0] >= (screen.get_rect().centery - BUTTON_WIDTH / 2) and pos[0] <= (screen.get_rect().centery + BUTTON_WIDTH / 2)) and (pos[1] >= (screen.get_rect().centerx - BUTTON_HEIGHT / 2) and pos[1] <= (screen.get_rect().centerx + BUTTON_HEIGHT / 2)):
                    frame = 1
                    color_button = GREEN
        # Set the screen background
        screen.fill(RED)

        # Button Next from frame 2
        pygame.draw.rect(screen, color_next_button, [int(screen.get_rect().centerx - BUTTON_NEXT_WIDTH / 2), screen.get_rect().centery + BUTTON_HEIGHT * 2, BUTTON_NEXT_WIDTH, BUTTON_NEXT_HEIGHT]) 

        # Button Start Game from frame 1 and space to put name in frame 2
        pygame.draw.rect(screen, color_button, [int(screen.get_rect().centerx - BUTTON_WIDTH / 2), int(screen.get_rect().centery - BUTTON_HEIGHT / 2), BUTTON_WIDTH, BUTTON_HEIGHT])

        clock.tick(10)

        pygame.display.flip()

# Be IDLE friendly. If you forget this line, the program will 'hang'
# on exit.
pygame.quit()
connection.close()
