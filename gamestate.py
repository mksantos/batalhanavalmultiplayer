import numpy as np
from random import *

class GameState:
    """
    Classe que representa o tabuleiro do inimigo.
    """


    # -------------------------------------------------
    def __init__(self, tam, player):
        """
        Construtor. Initializa o tabuleiro vazio.
        """
        self.score = 0
        self.attacked = 0
        self.player = player
        self.sub = [];
        self.cru = [];
        self.arm = [];
        self.air = [];

        self.tam = tam
        self.board = []
        for row in range(tam):
            self.board.append([])
            for column in range(tam):
                self.board[row].append(0)  # Append a cell
 

    # -------------------------------------------------
    def save(self):
        
        return ';'.join([';'.join(str(x)) for x in self.board])

        #return ';'.join(map(str, self.board)) 

    # -------------------------------------------------
    def restore(self, data):
        #self.board = np.reshape(data.split(';'), (3,3)).tolist()
        self.board = np.array(data.split(';')).tolist()
        new_board = []
        for i in self.board:
            if i == '0' or i == '1' or i == '2' or i == '3' or i == '5' or i == '4':
                new_board.append(int(i))
        new_board = np.array(new_board)
        if len(new_board) == 100:
            self.board = new_board.reshape(10,10).tolist()



    # -------------------------------------------------
    def printTab(self):

        print(self.board)
        

    # -------------------------------------------------
    #define o tabuleiro com os navios inimigos
    def defineTab(self, sub, cru, arm, air):


        self.sub = sub
        self.cru = cru
        self.arm = arm
        self.air = air    

    def attack(self, row, col):

        if self.board[row][col] == 3 or self.board[row][col] == 4:
            return 5

        else:    
            
            if self.board[row][col] == 1:
                self.score = self.score + 1
                self.board[row][col] = 3
                return 0
            else:
                self.board[row][col] = 4
            

    #testa se há um vencedor e portanto, se a partida acabou.
    def isFinished(self):
        if self.score == 4:
            print("O jogador %s ganhou a partida!" %(self.player))
            return True
        else:
            return False



    '''
    def moveRandom(self, piece):
        options = []
        for row in range(3):
            for col in range(3):
                if self.board[row][col] == '':
                    options.append((row, col))
        shuffle(options)
        if len(options) > 0:
            row = options[0][0]
            col = options[0][1]
            self.move(row, col, piece)
    '''

